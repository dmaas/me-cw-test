"""### ME CW Test

This profile is a simple demonstration of the use of Powder mobile endpoints
(MEs), which are identical collections of compute and SDR resources deployed on
campus buses with out-of-band LTE/WiFi access for experimental control.
Specifically, it uses the B210 SDR available at each ME to transmit a CW signal
within the LTE band 7 uplink (2500-2570 MHz) for real-time observation using
GnuRadio and an X310 SDR at one or more rooftop base stations.

Powder MEs traverse a variety of routes around the UofU campus. You can see
which MEs are available, along with their current locations, on this
[map](https://www.powderwireless.net/powder-map.php). You can toggle the
visiblility of each route and its associated MEs using the buttons in upper
right corner. Click on the label icon in the upper left corner to show ME IDs.
ME information is also available in the list found
[here](https://www.powderwireless.net/mobile-endpoints.php). The map and ME list
will be useful later for understanding where the MEs are in relation to the
rooftop base stations in your experiment.

The default parameters in this profile will allocate the following:

- Two rooftop base station X310 SDRs w/ associated compute resources (Browning, Hospital).
- All available Powder MEs.
- 10 MHz of spectrum in the LTE band 7 uplink (2500-2510 MHz).

Instructions:

1. Instantiate and parameterize your experiment. You may need to change the
default base station and spectrum parameters depending on availability.

2. Wait for the experiment to become ready and for the startup scrips to finish.
When the startup scripts are done, you'll see "Finished" in the Startup column
on List View tab of the experiment page.

3. Once all startup scripts are finished, log in to each ME and run
`/local/repository/bin/transmit-carrier.py`. This script will transmit a
different CW signal for each ME, starting near the lower end of the spectrum
range you selected, and increasing in frequency with the ME ID. For example, if
the ME IDs in your experiment are [4011, 4039, 4817], the CW signals for the MEs
will be transmitted at [f0, f0+df, f0+2*df] MHz respectively, where f0 and df
depend on the allocated spectrum (see the script for details). The script will
print each ME ID and CW frequency to stdout for convenience.

4. Log in to each rooftop compute node with X11 forwarding enabled and run
`uhd_fft -f{FREQ} -s{SAMP_RATE} -g25`, replacing {FREQ} with the midpoint in
your spectrum allocation (for default parameters, this would be 2505e6) and
{SAMP_RATE} with a value equal to the total bandwidth allocated for the
experiment (default would be 10e6). Switch to the waterfall view.

5. Now you can observe the relative received power corresponding to the ME CW
signals at at each base station. Going back to the map, you can understand the
recieved powers in terms of the positions of the MEs relative to the base
stations, and see how they change as the MEs traverse their routes around
campus.

Notes:

- There may be interference in the band 7 UL, but you should be able to witness
the CW signals in the uhd_fft waterfall regardless.

- This profile does not yet handle updating the CW frequencies for the MEs when
new MEs come online. If the roster of available MEs changes during your
experiment, you can just transmit CW signals manually with `uhd_siggen`.

"""

import geni.portal as portal
import geni.rspec.pg as pg
import geni.rspec.emulab.pnext as pn
import geni.rspec.igext as ig
import geni.rspec.emulab.spectrum as spectrum
import geni.rspec.emulab.route as route

class GLOBALS:
    ROOFTOP_IMG = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-STD"
    ME_IMG = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-STD"
    DLDEFLOFREQ = 2620.0
    DLDEFHIFREQ = 2630.0
    ULDEFLOFREQ = 2500.0
    ULDEFHIFREQ = 2510.0


def x310_node_pair(idx, x310_radio):
    radio_link = request.Link("radio-link-%d"%(idx))
    radio_link.bandwidth = 10*1000*1000

    node = request.RawPC("%s-comp"%(x310_radio.radio_name))
    node.hardware_type = params.x310_pair_nodetype
    node.disk_image = GLOBALS.ROOFTOP_IMG
    node.component_manager_id = "urn:publicid:IDN+emulab.net+authority+cm"
    node.addService(pg.Execute(shell="bash", command="/local/repository/bin/tune-sdr-iface.sh"))
    node.addService(pg.Execute(shell="bash", command="/local/repository/bin/setup-rooftop.sh"))

    node_radio_if = node.addInterface("usrp_if")
    node_radio_if.addAddress(pg.IPv4Address("192.168.40.1",
                                               "255.255.255.0"))
    radio_link.addInterface(node_radio_if)

    radio = request.RawPC("%s-x310"%(x310_radio.radio_name))
    radio.component_id = x310_radio.radio_name
    radio.component_manager_id = "urn:publicid:IDN+emulab.net+authority+cm"
    radio_link.addNode(radio)

node_type = [
    ("d740",
     "Emulab, d740"),
    ("d430",
     "Emulab, d430")
]

portal.context.defineParameter("x310_pair_nodetype",
                               "Type of compute node paired with the X310 Radios",
                               portal.ParameterType.STRING,
                               node_type[0],
                               node_type)

rooftop_names = [
    ("cellsdr1-browning",
     "Emulab, cellsdr1-browning (Browning)"),
    ("cellsdr1-bes",
     "Emulab, cellsdr1-bes (Behavioral)"),
    ("cellsdr1-dentistry",
     "Emulab, cellsdr1-dentistry (Dentistry)"),
    ("cellsdr1-fm",
     "Emulab, cellsdr1-fm (Friendship Manor)"),
    ("cellsdr1-honors",
     "Emulab, cellsdr1-honors (Honors)"),
    ("cellsdr1-meb",
     "Emulab, cellsdr1-meb (MEB)"),
    ("cellsdr1-smt",
     "Emulab, cellsdr1-smt (SMT)"),
    ("cellsdr1-hospital",
     "Emulab, cellsdr1-hospital (Hospital)"),
    ("cellsdr1-ustar",
     "Emulab, cellsdr1-ustar (USTAR)"),
]

portal.context.defineStructParameter("x310_radios",
                                     "X310 Radios",
                                     [{"radio_name": "cellsdr1-browning"},
                                      {"radio_name": "cellsdr1-hospital"}],
                                     multiValue=True,
                                     itemDefaultValue={},
                                     min=0, max=None,
                                     members=[
                                        portal.Parameter(
                                             "radio_name",
                                             "Rooftop base-station X310",
                                             portal.ParameterType.STRING,
                                             rooftop_names[0],
                                             rooftop_names)
                                     ])

portal.context.defineParameter(
    "ul_freq_min",
    "Uplink Frequency Min",
    portal.ParameterType.BANDWIDTH,
    GLOBALS.ULDEFLOFREQ,
    longDescription="Values are rounded to the nearest kilohertz."
)
portal.context.defineParameter(
    "ul_freq_max",
    "Uplink Frequency Max",
    portal.ParameterType.BANDWIDTH,
    GLOBALS.ULDEFHIFREQ,
    longDescription="Values are rounded to the nearest kilohertz."
)

params = portal.context.bindParameters()

# Check frequency parameters.
if params.ul_freq_min < 2500 or params.ul_freq_min > 2570 \
   or params.ul_freq_max < 2500 or params.ul_freq_max > 2570:
    perr = portal.ParameterError("Band 7 uplink frequencies must be between 2500 and 2570 MHz", ['ul_freq_min', 'ul_freq_max'])
    portal.context.reportError(perr)
if params.ul_freq_max - params.ul_freq_min < 1:
    perr = portal.ParameterError("Minimum and maximum frequencies must be separated by at least 1 MHz", ['ul_freq_min', 'ul_freq_max'])
    portal.context.reportError(perr)

# Now verify.
portal.context.verifyParameters()

# Lastly, request resources.
request = portal.context.makeRequestRSpec()

for i, x310_radio in enumerate(params.x310_radios):
    x310_node_pair(i, x310_radio)

all_routes = request.requestAllRoutes()
all_routes.disk_image = GLOBALS.ME_IMG;
all_routes.addService(pg.Execute(shell="bash", command="/local/repository/bin/setup-me.sh"))

request.requestSpectrum(params.ul_freq_min, params.ul_freq_max, 0)

portal.context.printRequestRSpec()
