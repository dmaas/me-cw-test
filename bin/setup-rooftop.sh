while ! wget -qO - http://repos.emulab.net/emulab.key | sudo apt-key add -
do
    echo Failed to get emulab key, retrying
    sleep 1
done

while ! sudo add-apt-repository -y http://repos.emulab.net/powder/ubuntu/
do
    echo Failed to get johnsond ppa, retrying
    sleep 1
done

while ! sudo apt-get update
do
    echo Failed to update, retrying
    sleep 1
done

while ! sudo DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends python3-uhd uhd-host libuhd-dev libuhd4.0.0 python3-lxml
do
    echo Failed to install dependencies, retrying
    sleep 1
done

while ! sudo DEBIAN_FRONTEND=noninteractive apt-get install -y gnuradio
do
    echo Failed to install gnuradio, retrying
    sleep 1
done

while ! sudo /usr/lib/uhd/utils/uhd_images_downloader.py
do
    echo Failed to download uhd images, retrying
    sleep 1
done
