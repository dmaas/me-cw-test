#!/usr/bin/env python3

import sys
import subprocess
from lxml import etree

tx_bw_buffer_ratio = 0.05
tx_gain = 85
sample_rate = 1e6
max_clients = 30

data_set_ns = {None: 'http://www.protogeni.net/resources/rspec/ext/profile-parameters/1'}

mfest = subprocess.run(['/usr/bin/geni-get', 'manifest'], stdout=subprocess.PIPE).stdout.decode('utf-8')
mfest_tree = etree.XML(mfest)
data_set = mfest_tree.find('data_set', data_set_ns)

# parse transmit spectrum range
ul_freq_min, ul_freq_max = None, None
for data_item in data_set:
    if data_item.get('name') == 'emulab.net.parameter.ul_freq_min':
        ul_freq_min = float(data_item.text) * 1e6
    elif data_item.get('name') == 'emulab.net.parameter.ul_freq_max':
        ul_freq_max = float(data_item.text) * 1e6

if ul_freq_min is None or ul_freq_max is None:
    sys.exit('Could not find frequency params in manifest! Exiting')

ul_bw = ul_freq_max - ul_freq_min
ul_bw_buf = tx_bw_buffer_ratio * ul_bw

# parse client_ids
my_client_id = subprocess.run(['/usr/bin/geni-get', 'client_id'],
                              stdout=subprocess.PIPE).stdout.decode('utf-8').rstrip()
my_bus_number = int(my_client_id.split('-')[-1])
print('My bus number: {}'.format(my_bus_number))

nodes = mfest_tree.findall('node', mfest_tree.nsmap)
bus_numbers = list()
for node in nodes:
    client_id = node.attrib['client_id']
    if 'bus' in client_id and client_id not in bus_numbers:
        bus_numbers.append(int(client_id.split('-')[-1]))

num_buses = len(bus_numbers)
cw_freqs = [ul_freq_min + ul_bw_buf / 2 + (ul_bw - ul_bw_buf) / max_clients * ind for ind in range(num_buses)]
bus_numbers.sort()
print('{} buses in experiment: '.format(num_buses))
print(bus_numbers)

my_ind = bus_numbers.index(my_bus_number)
my_cw_freq = cw_freqs[my_ind]
print('My index: {} -- transmitting tone at {}'.format(my_ind, my_cw_freq))
subprocess.call(['uhd_siggen',
                 '-f{}'.format(my_cw_freq),
                 '-s{}'.format(sample_rate),
                 '-g{}'.format(tx_gain)], stdout=subprocess.PIPE)
